/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.app.FragmentManager;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.example.android.bluetoothlegatt.BluetoothLeLocationService.EXTRA_DATA;
import static java.lang.Double.NaN;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeLocationService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class MapActivity extends FragmentActivity implements AnchorCallbacks{
    private final static String TAG = MapActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeLocationService mBluetoothLeLocationService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnectionAllowed = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private EditText mConsole = null;
    private int mNetworkSize = 0;
    GameSurfaceFragment gameFragment;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    // Code to manage Service lifecycle.
    private ServiceConnection mServiceConnection = null;
    private HashMap<String, BluetoothLeLocationService.Anchor> mNetwork;
    private float[] pos_hat = {0f, 0f};
    private double[] dummy_pos_hat = {30, 20};

    SharedPreferences sharedPref;

    FragmentManager fragmentManager;

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeLocationService.ACTION_GATT_CONNECTED.equals(action)) {
                String aLabel = intent.getStringExtra(EXTRA_DATA);
                mNetworkSize++;
                if(gameFragment != null && mNetwork != null) {
                    BluetoothLeLocationService.Anchor mAnchor = mNetwork.get(aLabel);
                    if(mAnchor != null){

                        String xValueKeyString = getString(R.string.saved_x_pos_pre_addr) + aLabel;
                        String yValueKeyString = getString(R.string.saved_y_pos_pre_addr) + aLabel;

                        float storedX = sharedPref.getFloat(xValueKeyString, mAnchor.x_pos);
                        float storedY = sharedPref.getFloat(yValueKeyString, mAnchor.y_pos);

                        mAnchor.x_pos=storedX;
                        mAnchor.y_pos=storedY;

                        gameFragment.addAnchor(new float[]{mAnchor.x_pos, mAnchor.y_pos},aLabel);
                    }
                }
                log("Connected with " + aLabel + ". NetworkSize = "+mNetworkSize);
            } else if (BluetoothLeLocationService.ACTION_GATT_DISCONNECTED.equals(action)) {
                 String aLabel = intent.getStringExtra(EXTRA_DATA);
                  mNetworkSize--;
                if(gameFragment != null) {
                    gameFragment.removeAnchor(aLabel);
                }
                  log("Disconnected from " + intent.getStringExtra(EXTRA_DATA) + ". NetworkSize = "+mNetworkSize);
            } else if (BluetoothLeLocationService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                log("Available services at peer discovered");
                //service check is automatically perfomred by the BluetoothLeLocationService
            } else if (BluetoothLeLocationService.ACTION_DATA_AVAILABLE.equals(action)) {
                float[] newPos = calculateMyCoordinates();
                log("New position is ("+String.format(Locale.ITALY,"%.2f", newPos[0])+", "+String.format(Locale.UK,"%.2f", newPos[1])+")");
                if(gameFragment != null) {
                    gameFragment.movePoint(newPos);
                }
//                String nLabel = intent.getStringExtra(EXTRA_DATA);
//                BluetoothLeLocationService.Anchor n = mNetwork.get(nLabel);
//                if(n != null){
//                    log("Node: "+ nLabel + " - D : "+n.getLastDistanceEstimation());
//                }

            }
            else if (BluetoothLeLocationService.ACTION_CONSOLE_MESSAGE_AVAILABLE.equals(action)) {
                String message = intent.getStringExtra(BluetoothLeLocationService.EXTRA_STRING);
                log(message);
            }
        }
    };

    //this was more elegant, but passing mAnchorCallbacks to the fragment is a mess
//    AnchorCallbacks mAnchorCallbacks = new AnchorCallbacks() {
//        @Override
//        public void onPositionChange(String label, float new_x, float new_y) {
//            BluetoothLeLocationService.Anchor an = mNetwork.get(label);
//
//            if(an != null){
//                an.x_pos = new_x;
//                an.y_pos = new_y;
//            }
//        }
//    };

    @Override
    public void onPositionChange(String label, float new_x, float new_y, float new_scale){
        if(label.equals("MAP")){
            Log.d(TAG, "onSurfaceChange. New pos= " + String.format(Locale.ITALY, "%.2f", new_x) +
                    ", " + String.format(Locale.ITALY, "%.2f", new_y) +
                    " New scale= " + String.format(Locale.ITALY, "%.2f", new_scale));

            SharedPreferences.Editor editor = sharedPref.edit();
            String mapName = getMapName();

            editor.putFloat(getString(R.string.saved_x_pos_map_pre_name)+mapName, (int)new_x);
            editor.putFloat(getString(R.string.saved_y_pos_map_pre_name)+mapName, (int)new_y);
            editor.putFloat(getString(R.string.saved_map_scale_pre_name)+mapName, new_scale);

            editor.commit();

            return;
        }
        else {
            BluetoothLeLocationService.Anchor an = mNetwork.get(label);
            if (an != null) {

                Log.d(TAG, "onSurfaceChange. New pos= " + String.format(Locale.ITALY, "%.2f", new_x) + ", " + String.format(Locale.ITALY, "%.2f", new_y));
                an.x_pos = new_x;
                an.y_pos = new_y;

                //store new position in permanent storage
                SharedPreferences.Editor editor = sharedPref.edit();
                String xValueKeyString = getString(R.string.saved_x_pos_pre_addr) + label;
                String yValueKeyString = getString(R.string.saved_y_pos_pre_addr) + label;

                editor.putFloat(xValueKeyString, new_x);
                editor.putFloat(yValueKeyString, new_y);
                editor.commit();

                pos_hat[0] = 0;
                pos_hat[1] = 0;

                return;
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate()");

        setContentView(R.layout.map_view);
        //if (savedInstanceState == null) {


        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        //addPreferencesFromResource(R.xml.preferences);

        sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        fragmentManager = getFragmentManager();

        //todo: in some way the mAnchorCallbacks has to be passed to gameFragment
        mConsole = (EditText) findViewById(R.id.console_item);

        getActionBar().setTitle(R.string.map_title);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent gattServiceIntent = new Intent(this, BluetoothLeLocationService.class);
        startService(gattServiceIntent);
        //bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        log("Activity created!");
        //}else{
        //    return;
        //}
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();

        // Create an instance of ExampleFragment
        gameFragment = new GameSurfaceFragment();
        fragmentManager.beginTransaction().add(R.id.fragment_container, gameFragment).commit();

        mServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder service) {
                log("BLE location service connected!");
                Log.d(TAG,"BLE location service connected!");
                if(mBluetoothLeLocationService == null) {
                    mBluetoothLeLocationService = ((BluetoothLeLocationService.LocalBinder) service).getService();

                    if (!mBluetoothLeLocationService.initialize()) {
                        Log.e(TAG, "Unable to initialize Bluetooth");
                        log("Unable to initialize Bluetooth");
                        finish();
                    }else{
                        log("Bluetooth initialized.");
                        Log.d(TAG,"Bluetooth initialized.");
                        mNetwork = mBluetoothLeLocationService.getNetork();
                        mConnectionAllowed = mBluetoothLeLocationService.getAdvertisingState();


                        Log.d(TAG,"mConnectionAllowed = "+mConnectionAllowed);
                        invalidateOptionsMenu();
                    }
                }

                mNetwork = mBluetoothLeLocationService.getNetork();

                if(gameFragment != null && mNetwork != null) {
                    String mapName = getMapName();

                    float width_stored = sharedPref.getFloat(getString(R.string.saved_map_width_pre_name)+mapName,52.4f);
                    float height_stored = sharedPref.getFloat(getString(R.string.saved_map_height_pre_name)+mapName,82.79f);
                    String mapPath=sharedPref.getString(getString(R.string.map_filename),getString(R.string.default_map_path));

                    loadMap(mapPath, width_stored, height_stored);

                    float x_offset_stored= sharedPref.getFloat(getString(R.string.saved_x_pos_map_pre_name)+mapName,0);
                    float y_offset_stored= sharedPref.getFloat(getString(R.string.saved_y_pos_map_pre_name)+mapName,0);
                    float scale_stored= sharedPref.getFloat(getString(R.string.saved_map_scale_pre_name)+mapName,1);

                    gameFragment.surface.setX_offset((int)x_offset_stored);
                    gameFragment.surface.setY_offset((int)y_offset_stored);
                    gameFragment.surface.resizeBackgroundMap(scale_stored);

                    Collection<BluetoothLeLocationService.Anchor> cNetwork =  mNetwork.values();
                    for (BluetoothLeLocationService.Anchor elem : cNetwork) {
                        if(elem != null){
                            gameFragment.addAnchor(new float[]{elem.x_pos, elem.y_pos},elem.mBluetoothDevice.toString());
                        }
                    }
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                log("BLE location service disconnected!");
                mBluetoothLeLocationService = null;
            }
        };


//        if(gameFragment != null && mNetwork != null) {
//            String mapPath = sharedPref.getString(getString(R.string.map_filename),getString(R.string.default_map_path_string));

//            try {
//                loadMap(mapPath);
//            }catch (Exception e){
//                Log.w(TAG,"no map image found!");
//
//                gameFragment.addMap(null, new int[]{0, 0}, 1, 0);
//            }

//            Collection<BluetoothLeLocationService.Anchor> cNetwork =  mNetwork.values();
//            for (BluetoothLeLocationService.Anchor elem : cNetwork) {
//                if(elem != null){
//                    gameFragment.addAnchor(new float[]{elem.x_pos, elem.y_pos},elem.mBluetoothDevice.toString());
//                }
//            }
//        }

        //INIZIALIZZA IL BIND
        Intent gattServiceIntent = new Intent(this, BluetoothLeLocationService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());


//        if (mBluetoothLeLocationService != null) {
            //final boolean result = mBluetoothLeLocationService.connect(mDeviceAddress);
            //Log.d(TAG, "Connect request result=" + result);
 //       }
    }

    private String getMapName(){
        String path = sharedPref.getString(getString(R.string.map_filename),getString(R.string.default_map_path_string));

        String path_parts[] = path.split("/");

        return path_parts[path_parts.length-1];
    }

    private void loadMap(String Path, float real_width, float real_height){
        Log.d(TAG,"loading map");

        Bitmap mapImage;
        try {
            mapImage = getBitmap(Path);
        }catch (Exception e){
            Log.w(TAG,"no map image found, loading the default one!");
            mapImage = BitmapFactory.decodeResource( getBaseContext().getResources(),R.drawable.fbk_nord_f2);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.map_filename),getString(R.string.default_map_path));

            real_width = 52.4f;
            real_height = 82.79f;
            String mapName = getMapName();
            editor.putFloat(getString(R.string.saved_map_width_pre_name)+mapName,real_width);
            editor.putFloat(getString(R.string.saved_map_height_pre_name)+mapName,real_height);

            editor.commit();
        }
        float scaling_m_each_pixel = (real_width/mapImage.getWidth() + real_height/mapImage.getHeight())/2;
        gameFragment.addMap(mapImage, new int[]{0, 0}, scaling_m_each_pixel, 0);
    }

    public Bitmap getBitmap(String path) throws FileNotFoundException {

        Bitmap bitmap = null;
        File f = new File(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        return bitmap;
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart()");
        super.onStart();
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();


    }

    @Override
    protected void onPause() {
        fragmentManager.beginTransaction().remove(gameFragment).commit();
        gameFragment=null;
        Log.d(TAG, "onPause()");
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mServiceConnection = null;
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");

        super.onDestroy();

        //unbindService(mServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        if (mConnectionAllowed) {
            menu.findItem(R.id.menu_allow_connection).setVisible(false);
            menu.findItem(R.id.menu_disallow_connection).setVisible(true);
            menu.findItem(R.id.menu_disconnect_all).setVisible(true);
            menu.findItem(R.id.menu_settings).setVisible(true);
        } else {
            menu.findItem(R.id.menu_allow_connection).setVisible(true);
            menu.findItem(R.id.menu_disallow_connection).setVisible(true);
            menu.findItem(R.id.menu_disconnect_all).setVisible(true);
            menu.findItem(R.id.menu_settings).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean retValue = false;
        switch(item.getItemId()) {
            case R.id.menu_allow_connection:
                mBluetoothLeLocationService.enable_connections();
                mConnectionAllowed = true;
                retValue = true;
                break;
            case R.id.menu_disconnect_all:
                mBluetoothLeLocationService.disconnect_all();
                retValue =  true;
                break;
            case R.id.menu_disallow_connection:
                mBluetoothLeLocationService.disable_connections();
                mConnectionAllowed = false;
                retValue =  true;
                break;
            case R.id.menu_settings:
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                retValue =  true;
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, DeviceScanActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
//                onBackPressed();
                retValue =  true;
                break;
        }
        if(retValue) {
            invalidateOptionsMenu();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) { //TODO: convert this to show the amount of connected nodes
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private float[] calculateMyCoordinates(){
        int noOfAnchors = mNetwork.size();

        final int WLSIterations=10;

        //prepare data for wls
        float[][] a_pos = new float[noOfAnchors][1];    //anchor positions in x,y format
        float[] d_hat = new float[noOfAnchors];         //distance from anchors
        Collection<BluetoothLeLocationService.Anchor> cNetwork =  mNetwork.values();
        int nodeIdx = 0;
        for (BluetoothLeLocationService.Anchor elem : cNetwork) {
            if(elem != null){
                d_hat[nodeIdx] = elem.getLastDistanceEstimation();
                //if(d_hat[nodeIdx] < 0)
                //    d_hat[nodeIdx] = 0;
                if( Float.isNaN(d_hat[nodeIdx])){
                    float[] ret = {0f/0f, 0f/0f};
                    return ret;                     //brief handle of NaNs. NaNs happens when the device is connected but it has still sto start sending data
                }

                a_pos[nodeIdx] = new float[]{elem.x_pos, elem.y_pos};
                nodeIdx++;
            }
        }

        //note: naming from now on is the same of the matlab version
        for(int iM = 0; iM < WLSIterations; iM++){
            // Data for the WLS
            double[][] H = new double[noOfAnchors][2];
            double[][] H_t = new double[2][noOfAnchors];
//            double[][] W = new double[noOfAnchors][noOfAnchors];
            double[] z = new double[noOfAnchors];
            double[] z_hat = new double[noOfAnchors];

            // computing the first loop:
            //                z = [z; d_hat{nodeIdx}(iT)];
            //                z_hat = [z_hat; sqrt((pos_hat(iT,1) - a_pos(nodeIdx,1))^2 + (pos_hat(iT,2) - a_pos(nodeIdx,2))^2)];
            //                H = [H; (pos_hat(iT,1) - a_pos(nodeIdx,1))/z_hat(end), (pos_hat(iT,2) - a_pos(nodeIdx,2))/z_hat(end)];
            for(nodeIdx = 0; nodeIdx < noOfAnchors; nodeIdx++){
                //if length(d_hat{nodeIdx}) >= iT && not(isnan(d_hat{nodeIdx}(iT))) && d_hat{nodeIdx}(iT) > 0.1 //linea presa da matlab, filtra alcuni outlier
                z[nodeIdx] = d_hat[nodeIdx];
                z_hat[nodeIdx] = sqrt( pow(pos_hat[0] - a_pos[nodeIdx][0],2) + pow(pos_hat[1] - a_pos[nodeIdx][1],2) );
                H[nodeIdx][0] = (pos_hat[0] - a_pos[nodeIdx][0])/z_hat[nodeIdx];
                H[nodeIdx][1] = (pos_hat[1] - a_pos[nodeIdx][1])/z_hat[nodeIdx];
                H_t[0][nodeIdx] = H[nodeIdx][0];
                H_t[1][nodeIdx] = H[nodeIdx][1];
            }

            //Compute the update value: G_hat = H'*inv(W)*H, but since W is eye, inv(W) = W and then H'*inv(W)*H = H'*H
            double[][] G_hat = {{0, 0},{0, 0}};
            for(nodeIdx = 0; nodeIdx < noOfAnchors; nodeIdx++){
                G_hat[0][0] = G_hat[0][0]+H[nodeIdx][0]*H_t[0][nodeIdx];
                G_hat[0][1] = G_hat[0][1]+H[nodeIdx][0]*H_t[1][nodeIdx];
                G_hat[1][0] = G_hat[1][0]+H[nodeIdx][1]*H_t[0][nodeIdx];
                G_hat[1][1] = G_hat[1][1]+H[nodeIdx][1]*H_t[1][nodeIdx];
            }

            //computing Delta_pos = inv(G_hat)*H'*inv(W)*(z - z_hat)
            double k = 1/(G_hat[0][0]*G_hat[1][1] - G_hat[0][1]*G_hat[1][0]);
            double[][] G_hat_inv = {{k*G_hat[1][1], -k*G_hat[0][1]},{-k*G_hat[1][0], k*G_hat[0][0]}};
            double[][] temp1 = new double[2][noOfAnchors];
            for(nodeIdx = 0; nodeIdx < noOfAnchors; nodeIdx++){
                temp1[0][nodeIdx] = G_hat_inv[0][0]*H_t[0][nodeIdx] + G_hat_inv[0][1]*H_t[1][nodeIdx];
                temp1[1][nodeIdx] = G_hat_inv[1][0]*H_t[0][nodeIdx] + G_hat_inv[1][1]*H_t[1][nodeIdx];
            }
            float[] Delta_pos = {0, 0};
            for(nodeIdx = 0; nodeIdx < noOfAnchors; nodeIdx++){
                Delta_pos[0] = Delta_pos[0] + (float)(temp1[0][nodeIdx]*(z[nodeIdx]-z_hat[nodeIdx]));
                Delta_pos[1] = Delta_pos[1] + (float)(temp1[1][nodeIdx]*(z[nodeIdx]-z_hat[nodeIdx]));
            }

            if(Float.isNaN(Delta_pos[0]) || Float.isNaN(Delta_pos[0])){
                float[] ret = {0f/0f, 0f/0f};
                return ret; //brief handle of NaNs. NaNs happens when the device is connected but it has still sto start sending data
            }

            pos_hat[0] = pos_hat[0] + Delta_pos[0];
            pos_hat[1] = pos_hat[1] + Delta_pos[1];
        }

        return pos_hat;
    }


    private void log(final String txt) {
        if(mConsole == null) return;

        final String timestamp = new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Editable oldText = mConsole.getText();
                int size = oldText.length();
                if(size > 1000)
                    size = 1000;

                char toKeep[] = new char[size+1];
                oldText.getChars(0, size, toKeep, 0);
                String stringToKeep = new String(toKeep);


                String newText = timestamp + " : " + txt + "\n" + stringToKeep;
                mConsole.setText(newText);
            }
        });
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeLocationService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeLocationService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeLocationService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeLocationService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeLocationService.ACTION_CONSOLE_MESSAGE_AVAILABLE);
        return intentFilter;
    }
}
