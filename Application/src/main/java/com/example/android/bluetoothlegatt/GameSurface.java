package com.example.android.bluetoothlegatt;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback {

    private GameThread gameThread;
    private List<MovingCharacter> chibiList = new ArrayList<MovingCharacter>();
    private List<StaticCharacter> staticCharList = new ArrayList<StaticCharacter>();
    float x_min_meters = -20;
    float x_max_meters = 20;
    float y_min_meters = -20;
    float y_max_meters = 20;

    private int _xDelta;
    private int _yDelta;

    private int x_offset=0;
    private int y_offset=0;

    AnchorCallbacks callbackObj = null;

    private final static String TAG = "GameSurface";

    public GameSurface(Context context, AnchorCallbacks cb)  {
        super(context);
        Log.d(TAG, "GameSurface constructor called");

        // Make Game Surface focusable so it can handle events. .
        this.setFocusable(true);

        // Sét callback.
        this.getHolder().addCallback(this);

        // Save callback object
        callbackObj = cb;
    }

    public int getX_offset(){
        return x_offset;
    }

    public int getY_offset(){
        return y_offset;
    }

    public void setX_offset(int new_x_offset){
        x_offset = new_x_offset;
        return;
    }

    public void setY_offset(int new_y_offset){
        y_offset = new_y_offset;
        return;
    }

    public void update()  {
//        for(StaticCharacter character: staticCharList)  {
//            character.update();
//        }

        for(MovingCharacter chibi: chibiList) {
            chibi.update();
        }
    }

    public void updateVector(MovingCharacter chibi, float[] newPosition){
        int[] p = mToPixel(newPosition);
        chibi.updateVector(p);
    }

    public void updatePosition(StaticCharacter character, float[] newPosition){
        if(character == null)
            return;

        int[] p = mToPixel(newPosition);
        character.setXY(p[0],p[1]);
    }

//    public MovingCharacter getMainChibi(){
//        return getMovingObject("ME");
//    }

    public StaticCharacter getMainChibi(){
        return getStaticObject("ME");
    }

    private boolean actionMoveEvent(int prev_x, int prev_y, int new_x, int new_y){
        Iterator<StaticCharacter> iterator= this.staticCharList.iterator();
        while (iterator.hasNext()) {

            StaticCharacter character = iterator.next();

            if (!(character.getObjectLabel().equals("MAP") || character.getObjectLabel().equals("ME")) ) {

                float charLlim = (character.getX() - this.getX_offset())*this.getMapRatio() - character.getWidth()/2;// - character.getWidth()/2;
                float charRlim = charLlim + character.getWidth();
                float charTlim = (character.getY() - this.getY_offset())*this.getMapRatio() - character.getHeight()/2;// - character.getHeight()/2;
                float charBlim = charTlim + character.getHeight();

                if (prev_x > charLlim && prev_x < charRlim
                        && prev_y > charTlim && prev_y < charBlim) {

                    character.setXY(character.getX() + (int)(new_x/this.getMapRatio()), character.getY() + (int)(new_y/this.getMapRatio()));

                    float[] map_real_size = this.getMapObject().getRealSize();
                    int[] map_onscreen_size = new int[]{this.getMapObject().getWidth(), this.getMapObject().getHeight()};

                    if(callbackObj != null) {
                        float x_m = (character.getX()) * map_real_size[0] / map_onscreen_size[0];
                        float y_m = (character.getY()) * map_real_size[1] / map_onscreen_size[1];
                        callbackObj.onPositionChange(character.getObjectLabel(), x_m, y_m,1);
                    }
                    return true;
                }
            }
        }

        //if none of anchors where the target of the finger then it was the map
        this.setX_offset((int)((this.getX_offset()-new_x/this.getMapRatio())));
        this.setY_offset((int)((this.getY_offset()-new_y/this.getMapRatio())));

        callbackObj.onPositionChange("MAP", this.getX_offset(), this.getY_offset(),this.getMapRatio());

        return true;
    }

    private int mPtrCount = 0;
    private final static int maxTouches = 5;
    private int[] touchEventIdxs = new int[maxTouches];
    double distance = -1;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        touchEventIdxs[mPtrCount] = event.getActionIndex();

        int X = (int)event.getX(touchEventIdxs[mPtrCount]);
        int Y = (int)event.getY(touchEventIdxs[mPtrCount]);

        boolean ret = false;

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mPtrCount++;

                if(mPtrCount >= maxTouches)
                    return false;

                _xDelta = X;// - lParams.leftMargin;
                _yDelta = Y;// - lParams.topMargin;
                ret = true;
                break;
            case MotionEvent.ACTION_UP:
                mPtrCount--;
                distance = -1; //invalidate distance value
                if(mPtrCount == 1) {
                    int remaining_index;
                    if(event.getActionIndex() == 0)
                        remaining_index=1;
                    else
                        remaining_index=0;
                    _xDelta = (int)event.getX(remaining_index);
                    _yDelta = (int)event.getY(remaining_index);
                    ret = true;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                mPtrCount++;

                if(mPtrCount >= maxTouches)
                    return false;

                _xDelta = X;
                _yDelta = Y;
                ret = true;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                mPtrCount--;
                distance = -1; //invalidate distance value
                if(mPtrCount == 1) {
                    int remaining_index;
                    if(event.getActionIndex() == 0)
                        remaining_index=1;
                    else
                        remaining_index=0;
                    _xDelta = (int)event.getX(remaining_index);
                    _yDelta = (int)event.getY(remaining_index);
                    ret = true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(mPtrCount >= maxTouches)
                    return false;

                if(mPtrCount == 1) { //move action
                    ret = actionMoveEvent(X, Y, X - _xDelta, Y - _yDelta);
                    _xDelta = X;
                    _yDelta = Y;
                }else if(mPtrCount == 2) { //pinch zoom action, do not work properly (zoom and drag are not properly scaled)

                    int X1 = (int)event.getX(touchEventIdxs[mPtrCount-2]);
                    int Y1 = (int)event.getY(touchEventIdxs[mPtrCount-2]);
                    int X2 = (int)event.getX(touchEventIdxs[mPtrCount-1]);
                    int Y2 = (int)event.getY(touchEventIdxs[mPtrCount-1]);
                    int X_bar = (X1+X2)/2;
                    int Y_bar = (Y1+Y2)/2;

                    double new_distance = Math.sqrt(Math.pow(X1-X2,2) + Math.pow(Y1-Y2,2));
                    if(distance > 0){
                        double ratio_distance = (new_distance - distance)/distance;
                        this.resizeBackgroundMap(1+(float)(ratio_distance/2));

                        this.setX_offset((int)(this.getX_offset()+(_xDelta-X_bar)/this.getMapRatio()));
                        this.setY_offset((int)(this.getY_offset()+(_yDelta-Y_bar)/this.getMapRatio()));

                        ret = true;
                    }
                    _xDelta = X_bar;
                    _yDelta = Y_bar;
                    distance=new_distance;
                }
                break;
        }

        if(ret)
            this.invalidate();

        return ret;
    }

    @Override
    public void draw(Canvas canvas)  {
        super.draw(canvas);

        for(StaticCharacter character: staticCharList)  {
            character.draw(canvas);
        }

        for(MovingCharacter chibi: chibiList)  {
            chibi.draw(canvas);
        }
    }

    // Implements method of SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated()");
        if(this.getMainChibi() == null){
            Log.d(TAG,"Main chibi not fount. Adding it");

            //Bitmap chibiBitmap1 = BitmapFactory.decodeResource(this.getResources(),R.drawable.chibi1);
            //MovingCharacter mainChibi = new MovingCharacter( this,"ME",chibiBitmap1,100,50,0.35f);
            //this.chibiList.add(mainChibi);

            Bitmap chibiBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(this.getResources(),R.drawable.gps_position_target), 64 , 64, true);;
            StaticCharacter mainChibi = new StaticCharacter( this,"ME",chibiBitmap,0,0);

            this.staticCharList.add(mainChibi);
        }

        this.gameThread = new GameThread(this,holder);
        this.gameThread.setRunning(true);
        this.gameThread.start();

        //dummyResize();
    }

//    static float ratio = 2;
//    private void dummyResize(){
//        this.resizeBackgroundMap(ratio++);
//        this.invalidate();
//
//        Handler h = new Handler();
//        h.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                dummyResize();
//            }
//        }, 5000);
//    }


    // Implements method of SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    // Implements method of SurfaceHolder.Callback
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry= true;
        while(retry) {
            try {
                this.gameThread.setRunning(false);

                // Parent thread must wait until the end of GameThread.
                this.gameThread.join();
                retry= false;
            }catch(InterruptedException e)  {
                e.printStackTrace();
            }
        }
    }

    public void addChibi(float[] apos, String label, float vel){
        Bitmap chibiBitmap2 = BitmapFactory.decodeResource(this.getResources(),R.drawable.chibi2);
        int[] p = mToPixel(apos);
        MovingCharacter chibi = new MovingCharacter(this,label,chibiBitmap2,p[0],p[1], vel);

        this.chibiList.add(chibi);
    }

    private boolean isAlreadyThere(String nodeLabel){
        return getStaticObject(nodeLabel) != null;
    }

    public void addStaticChar(float[] apos, String label){

        if(getMapObject() != null) {
            for (int i = 0; i < staticCharList.size(); i++) {
                if (staticCharList.get(i).getObjectLabel().equals(label)) {
                    staticCharList.remove(i);
                    break;
                }
            }
        }
        Bitmap thingyBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.thingy);
        int[] p = mToPixel(apos);
        StaticCharacter chibi = new StaticCharacter(this, label, thingyBitmap, p[0], p[1]);

        this.staticCharList.add(chibi);

        reorderStaticCharList();
    }

    private void reorderStaticCharList(){
        List<StaticCharacter> newList = new ArrayList<StaticCharacter>();
        StaticCharacter temp = getMapObject();
        if(temp == null)
            return;
        newList.add(temp);

        Iterator<StaticCharacter> iterator = this.staticCharList.iterator();
        while (iterator.hasNext()) {

            temp = iterator.next();

            if( !(temp.equals("ME") || temp.equals("MAP")))
                newList.add(temp);
        }

        temp = getMainChibi();
        if(temp == null)
            return;
        newList.add(temp);

        staticCharList.clear();
        staticCharList = newList;
    }

    private float mapAspectRatio=-1;
    public void addBackgroundMap(Bitmap mapBitmap, int[] pos, float scalingFactor, float rotation ,String label){ //todo: remove scaling factor or rotation?

        if(getMapObject() != null) {
            for (int i = 0; i < staticCharList.size(); i++) {
                if (staticCharList.get(i).getObjectLabel().equals("MAP")) {
                    staticCharList.remove(i);
                    break;
                }
            }
        }
        //Bitmap mapBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.fbk_nord_f2);
        int scalingFactor_int = 1;

        float realMapSizeX = mapBitmap.getWidth()*scalingFactor;
        float realMapSizeY = mapBitmap.getHeight()*scalingFactor;

        //float realMapSizeX = 87.9f;
        //float realMapSizeY = 65.9f;

        mapAspectRatio = 1;

        //StaticCharacter map = new StaticCharacter(this,label,Bitmap.createScaledBitmap(mapBitmap, mapBitmap.getWidth()/scalingFactor_int, mapBitmap.getHeight()/scalingFactor_int, true),pos[0],pos[1], realMapSizeX, realMapSizeY );
        StaticCharacter map = this.getMapObject();
        if (map == null) {
//            if (mapBitmap == null) {
//                //mapBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fbk_s_croce);
//                mapBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.fbk_nord_f2);
//            }
            map = new StaticCharacter(this, label, Bitmap.createScaledBitmap(mapBitmap, mapBitmap.getWidth() / scalingFactor_int, mapBitmap.getHeight() / scalingFactor_int, true), pos[0], pos[1], realMapSizeX, realMapSizeY);
            this.staticCharList.add(map);

            //resizeBackgroundMap(scalingFactor);
        } else {
//            if (mapBitmap == null) {
//                mapBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fbk_nord_f2);
//            }
            map.setBitmap(mapBitmap);
        }

    }

    public void resizeBackgroundMap(float newScale){

        StaticCharacter map = getMapObject();

        if(map == null)
            return;

       // map.setRatio(newScale);
        mapAspectRatio=mapAspectRatio*newScale;
    }

    public float getMapRatio(){
        return mapAspectRatio;
    }

    public int[] mToPixel(float[] val){

        StaticCharacter map = this.getMapObject();
        if(map == null)
            return new int[]{(int)val[0] , (int)val[1]};

        int[] ret = new int[2];
        float[] mapRealSize = map.getRealSize();


        ret[0] = (int)((map.getWidth())/mapRealSize[0]*val[0]);
        ret[1] = (int)((map.getHeight())/mapRealSize[1]*val[1]);

        return ret;
    }

    public void removeObject(String label){
        for(int i = 0; i < this.staticCharList.size(); i++){
            if(this.staticCharList.get(i).getObjectLabel().equals(label)){
                this.staticCharList.remove(i);
                return;
            }
        }

        for(int i = 0; i < this.chibiList.size(); i++){
            if(this.chibiList.get(i).getObjectLabel().equals(label)){
                this.chibiList.remove(i);
                return;
            }
        }
    }

    private StaticCharacter getStaticObject(String label) {
        Iterator<StaticCharacter> iterator = this.staticCharList.iterator();
        while (iterator.hasNext()) {

            StaticCharacter character = iterator.next();

            if(character.getObjectLabel().equals(label))
                return character;
        }

        return null;
    }

    private MovingCharacter getMovingObject(String label) {
        Iterator<MovingCharacter> iterator = this.chibiList.iterator();
        while (iterator.hasNext()) {

            MovingCharacter character = iterator.next();

            if(character.getObjectLabel().equals(label))
                return character;
        }

        return null;
    }
    private StaticCharacter getMapObject() {
        return getStaticObject("MAP");
    }
}