package com.example.android.bluetoothlegatt;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GameSurfaceFragment extends Fragment {

    final static String TAG = "GameSurfaceFragment";
    GameSurface surface;
    AnchorCallbacks mCallback;


    @Override
    @TargetApi(23)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView()");
        if (savedInstanceState == null) {

            surface = new GameSurface(getContext(), mCallback);
            return surface;
        }else {
            return null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (AnchorCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    void movePoint(float[] newPos){
        if(surface == null)
            return;

        //surface.updateVector(surface.getMainChibi(), newPos);
        surface.updatePosition(surface.getMainChibi(), newPos);
    }

    void addAnchor(float[] apos, String label){
        surface.addStaticChar(apos, label);
    }

    void addMap(Bitmap map, int[] pos, float scalingFactor, double rotation){
        surface.addBackgroundMap(map, pos, scalingFactor, 0, "MAP");
    }

    void removeAnchor(String label){
        surface.removeObject(label);
    }
//@Override
//public void onCreate(Bundle savedInstanceState) {
//    // TODO Auto-generated method stub
//    super.onCreate(savedInstanceState);
//
//    //The heart and mind of headless fragment is below line. It will keep the fragment alive during configuration change when activities and   //subsequent fragments are "put to death" and recreated
//    setRetainInstance(true);
//}

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"Game surface fragment destroyed");
        //surface.update();
    }
}