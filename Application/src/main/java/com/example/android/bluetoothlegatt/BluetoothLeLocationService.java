/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.example.android.bluetoothlegatt.BluetoothLeLocationService.AdvetisingState.ADVERTISING_ENABLED;
import static com.example.android.bluetoothlegatt.BluetoothLeLocationService.AdvetisingState.ADVERTISING_IS_ENABLING;
import static com.example.android.bluetoothlegatt.BluetoothLeLocationService.AdvetisingState.ADVERTISING_NOT_ENABLED;
import static java.lang.Double.NaN;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeLocationService extends Service {
    private final static String TAG = "BLE LOCATION SERVICE";

    public enum AdvetisingState {
        ADVERTISING_NOT_ENABLED,
        ADVERTISING_ENABLED,
        ADVERTISING_IS_ENABLING,
    }

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;
    private BluetoothGattServer mBluetoothGattServer;
    private HashMap<String, Anchor> mNetwork;
    private AdvetisingState mAdvertisingState = ADVERTISING_NOT_ENABLED;
    private String mBluetoothDeviceAddress;
    //private BluetoothGatt mBluetoothGatt;
    //private int mConnectionState = STATE_DISCONNECTED;
    private int mBluetoothState = BluetoothAdapter.STATE_OFF;
    private String originalDeviceName;
    private String CLIMBL_NAME = "CLIMBL";

    private boolean isFirstCallToInitialize = true;

//    private static final int STATE_DISCONNECTED = 0;
//    private static final int STATE_CONNECTING = 1;
//    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED = //TODO: names are not correct. Here we use the broadcast not only for GATT messages.
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = //TODO: names are not correct. Here we use the broadcast not only for GATT messages.
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = //TODO: names are not correct. Here we use the broadcast not only for GATT messages.
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String ACTION_CONSOLE_MESSAGE_AVAILABLE =
            "com.example.bluetooth.le.ACTION_CONSOLE_MESSAGE_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    public final static String EXTRA_STRING =
            "com.example.bluetooth.le.EXTRA_STRING";

    public final static UUID UUID_RANGE_CHARACTERISTIC =
            UUID.fromString(SampleGattAttributes.UUID_RANGE_CHARACTERISTIC);
    public final static UUID UUID_DIST_SERVICE =
            UUID.fromString(SampleGattAttributes.UUID_DIST_SERVICE);

    public enum ErrorCode {
        NO_ERROR,
        WRONG_BLE_NAME_ERROR,
        ADVERTISER_NOT_AVAILABLE_ERROR,
        INTERNAL_ERROR,
        ANDROID_VERSION_NOT_COMPATIBLE_ERROR,
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Connected to GATT server on: "+gatt.getDevice().toString());
                log("Connected to GATT server on: "+gatt.getDevice().toString());

                Anchor mAnchor = mNetwork.get(gatt.getDevice().toString());
                if(mAnchor == null){
                    mAnchor = new Anchor(gatt, null, null,gatt.getDevice());
                }else{
                    mAnchor.mBluetoothGattClient = gatt;
                }
                mNetwork.put(gatt.getDevice().toString(),mAnchor);
                // Attempts to discover services after successful connection.
                if(mAnchor.mBluetoothGattClient != null && (mAnchor.mDistanceService == null || mAnchor.mRangeCharacteristic == null)) {
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mAnchor.mBluetoothGattClient.discoverServices());
                }else{
                    Log.d(TAG, "services already available for this device, just enable the notifications");
                    enableNotificationForRange(mAnchor);
                }
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");

//                Anchor mAnchor = mNetwork.get(gatt.getDevice().toString());
//                if(mAnchor != null){
//                    mAnchor.mBluetoothGattClient.disconnect();
//                    mAnchor.mBluetoothGattClient.close();
//                    mNetwork.remove(gatt.getDevice().toString());
//                }
//
//                broadcastUpdate(intentAction, gatt.getDevice().toString());

            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                Anchor mAnchor = mNetwork.get(gatt.getDevice().toString());
                if(mAnchor == null){
                    mAnchor = new Anchor(gatt, null, null,gatt.getDevice());
                    getDistanceService(mAnchor);
                }else{
                    mAnchor.mBluetoothGattClient = gatt;
                    getDistanceService(mAnchor);
                }
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
                log("onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, gatt.getDevice().getAddress(), characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            //Log.d(TAG, "onCharacteristicChanged");
//            int d = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0);
//
//            String devLabel = gatt.getDevice().getName();
//            if(devLabel == null)
//                devLabel = gatt.getDevice().toString();
//            log("D: " + devLabel + " " + ((double)d)/10 + "m" );
            Anchor mAnchor = mNetwork.get(gatt.getDevice().toString());
            if(mAnchor != null){
                mAnchor.last_distance = ((float)characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0))/10;
                log("New distance from "+mAnchor.mBluetoothDevice.toString()+" is " + String.format(Locale.ITALY,"%.2f", mAnchor.last_distance));
            }else{
                Log.w(TAG, "A notification has been received from an unknown node...");
            }
            broadcastUpdate(ACTION_DATA_AVAILABLE, gatt.getDevice().toString());
        }
    };

    public boolean getDistanceService(Anchor mAnchor) {
        Log.i(TAG, "Getting Distance Service");
        if(mAnchor.mBluetoothGattClient == null)
            return false;

        mAnchor.mDistanceService = mAnchor.mBluetoothGattClient.getService(UUID_DIST_SERVICE);

        if(mAnchor.mDistanceService == null) {
            Log.e(TAG, "Could not get DISTANCE Service");
            log("Could not get DISTANCE Service");
            return false;
        }
        else {
            Log.i(TAG, "DISTANCE Service successfully retrieved");
            log("DISTANCE Service successfully retrieved");
            if(getRangeCharacteristic(mAnchor)){
                Log.i(TAG,"DISTANCE service fully acquired with characteristics");
                log("DISTANCE service fully acquired with characteristics");
                if(enableNotificationForRange(mAnchor)){
                    log("Notification enabled properly, dev: "+mAnchor.mBluetoothGattClient.getDevice().toString()+" shall start soon sending data.");
                    return true;
                }
                return false;
            } else {
                Log.i(TAG,"Could not get DISTANCE characteristics");
                return false;
            }
        }
    }

    private boolean getRangeCharacteristic(Anchor mAnchor) {
        Log.i(TAG, "Getting CIPO characteristic");
        if(mAnchor.mDistanceService == null) {
            Log.e(TAG, "mDistanceService is null, cannot search characteristic");
            return false;
        }

        mAnchor.mRangeCharacteristic = mAnchor.mDistanceService.getCharacteristic(UUID_RANGE_CHARACTERISTIC);

        if(mAnchor.mRangeCharacteristic == null) {
            Log.i(TAG, "Could not find RANGE Characteristic");
            log("Could not find RANGE Characteristic");
            return false;
        }
        else {
            Log.i(TAG, "RANGE characteristic retrieved properly");

            return true;
        }
    }

    private boolean enableNotificationForRange(Anchor mAnchor) {

        Log.i(TAG, "Enabling notification on Android API for Range characteristic");
        if(mAnchor == null || mAnchor.mRangeCharacteristic == null || mAnchor.mBluetoothGattClient == null || mAnchor.mDistanceService == null){
            Log.w(TAG, "Something == null !!");
            return false;
        }

        boolean success = mAnchor.mBluetoothGattClient.setCharacteristicNotification(mAnchor.mRangeCharacteristic, true);
        if(!success) {
            Log.i(TAG, "Enabling Android API notification failed!");
            return false;
        }
        else{
            Log.i(TAG, "Notification enabled on Android API!");
        }

        BluetoothGattDescriptor descriptor = mAnchor.mRangeCharacteristic.getDescriptor(UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        if(descriptor != null) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mAnchor.mBluetoothGattClient.writeDescriptor(descriptor);
            Log.i(TAG, "Notification on remote device (CCCD) enabled.");
        }
        else {
            Log.i(TAG, "Could not get descriptor for characteristic! CCCD Notification are not enabled.");
            return false;
        }
        return true;
    }


    private AdvertiseCallback mAdvCallback;
    class myAdvCallback extends AdvertiseCallback {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
            mAdvertisingState = ADVERTISING_ENABLED;
        }

        @Override
        public void onStartFailure(int errorCode) {
            Log.e( TAG, "Advertising onStartFailure: " + errorCode );
            super.onStartFailure(errorCode);
            mAdvertisingState = ADVERTISING_NOT_ENABLED;
            //TODO: handle error
            //disableMaintenanceProcedure(); //if the advertising fails disable maintenance so that the name keeps correct
        }
    };

//    private final BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() { //the app should subscribe to this, calling init() as soon it receives BluetoothAdapter.STATE_ON
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            final String action = intent.getAction();
//
//            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
//                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
//                //final BluetoothDevice device = intent.getExtras().getParcelable(BluetoothDevice.EXTRA_DEVICE);
//                //final String deviceName = device.getName();
//                Log.i(TAG,"Bluetooth_State_change, new state: " + state);
//                mBluetoothState = state;
//            }
//        }
//    };

    private final BluetoothGattServerCallback mBluetoothGattServerCallback = new BluetoothGattServerCallback(){
        public void  onConnectionStateChange(BluetoothDevice device, int status, int newState){
            Anchor mAnchor;
            String intentAction;
            switch (newState){
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i(TAG,"Device connected as peripheral...");
                    mAnchor = mNetwork.get(device.toString());
                    if(mAnchor == null){
                        mAnchor = new Anchor(device);
                    }
                    mNetwork.put(device.toString(),mAnchor);

                    intentAction = ACTION_GATT_CONNECTED;
                    broadcastUpdate(intentAction, device.toString());

                    device.connectGatt(getApplicationContext(),false, mGattCallback);
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.i(TAG,"Device disconnected as peripheral...");
                //case BluetoothProfile.STATE_DISCONNECTING:
                    mAnchor = mNetwork.get(device.toString());
                    if(mAnchor != null){
                        mAnchor.mBluetoothGattClient.disconnect();
                        mAnchor.mBluetoothGattClient.close();
                        mNetwork.remove(device.toString());
                    }
                    intentAction = ACTION_GATT_DISCONNECTED;
                    broadcastUpdate(intentAction, device.toString());
                    break;
                default:
                    break;
            }
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final String deviceLabel) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA, deviceLabel);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final String deviceLabel,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        if (UUID_DIST_SERVICE.equals(characteristic.getUuid())) {

            final String raw_data = "Dev: " + deviceLabel + ", dist: " + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            intent.putExtra(EXTRA_DATA, raw_data);
        } else {
            //not intereseted in other kind of data
            return;
        }
        sendBroadcast(intent);
    }


    public class LocalBinder extends Binder {
        BluetoothLeLocationService getService() {
            return BluetoothLeLocationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "ClimbService created");

        mNetwork = new HashMap<String, Anchor>();

        mBinder = new LocalBinder();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind()");
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        mNetwork.clear();
        mNetwork = null;
    }

    private IBinder mBinder;// = new LocalBinder();

    private void log(String message){
        final Intent intent = new Intent(ACTION_CONSOLE_MESSAGE_AVAILABLE);
        String formattedMessage = "BLE service: "+message;
        intent.putExtra(EXTRA_STRING, formattedMessage);
        sendBroadcast(intent);
    }

    private boolean start_advertising() {
        Log.d(TAG, "start_advertising()");
        if (mBluetoothAdapter == null || mBluetoothLeAdvertiser == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                .setConnectable(true)
                .build();

        AdvertiseData advertiseData = new AdvertiseData.Builder()
                .setIncludeDeviceName(true) //TODO: FIND A BETTER WAY TO MAKE THE PHONE DISCOVERABLE BY THE ANCHOR (maybe with uuid?)
                .build();

        mAdvCallback = new myAdvCallback();
        //insertTag("enabling_advertise");
        mBluetoothLeAdvertiser.startAdvertising(settings, advertiseData, mAdvCallback);

        return true;
    }

    private boolean stop_advertising() {
        Log.d(TAG, "stop_advertising()");

        if(mBluetoothLeAdvertiser != null){
            if(mAdvertisingState==ADVERTISING_ENABLED || mAdvertisingState==ADVERTISING_IS_ENABLING)
                mBluetoothLeAdvertiser.stopAdvertising(mAdvCallback);

            mAdvertisingState = ADVERTISING_NOT_ENABLED;
        }

        return true;
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        Log.d(TAG, "initialize()");

        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
                return false;
            }
        }

        mBluetoothState = mBluetoothAdapter.getState();
        if(mBluetoothState != BluetoothAdapter.STATE_ON){
            Log.e(TAG, "The bluetooth seems to be not enabled.");
            return false;
        }

        if(mBluetoothGattServer == null) {
            mBluetoothGattServer = mBluetoothManager.openGattServer(getApplicationContext(), mBluetoothGattServerCallback);
        }

//        // Register broadcasts receiver for bluetooth state change
//        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
//        registerReceiver(mBluetoothStateReceiver, filter);

//        if (mBluetoothAdapter == null) {
//            Log.w(TAG, "mBluetoothAdapter == null");
//            return false; //internal error
//        }
        if(mBluetoothState != BluetoothAdapter.STATE_ON){
            Log.w(TAG, "the bluetooth is not enabled");
            return false;
        }

        if (mBluetoothLeAdvertiser == null ) {
            mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
            if (mBluetoothLeAdvertiser == null) {
                Log.w(TAG, "mBluetoothLeAdvertiser == null");
                return false;
            }

            originalDeviceName = mBluetoothAdapter.getName();

        }else{
            Log.i(TAG, "maintenance procedure already enabled");
        }

        if (!mBluetoothAdapter.isMultipleAdvertisementSupported()) {
            Log.w(TAG, "multiple advertisement not supported");
            //return ErrorCode.ADVERTISER_NOT_AVAILABLE_ERROR; //do not return for this. Try to change the name anyway, eventually it will return with WRONG_BLE_NAME_ERROR
        }

        String deviceName = mBluetoothAdapter.getName();

        if(deviceName != null && !deviceName.equals(CLIMBL_NAME)) {
            if(!mBluetoothAdapter.setName(CLIMBL_NAME)) {
                Log.w(TAG, "the method setName returned false");
                //insertTag("Cannot_set_name_advertise_not_enabled");
                return false; //wrong BLE name, the  setName can't update it!
            }
            //check the name string after the setting it....not strictly needed.
            deviceName = mBluetoothAdapter.getName();
            if(deviceName != null && !deviceName.equals(CLIMBL_NAME)) {
                Log.w(TAG, "BLE name check failed! Name not changed");
                //insertTag("Cannot_set_name_advertise_not_enabled");
                mBluetoothAdapter.setName(originalDeviceName);
                return false;
            }
        }

        if(isFirstCallToInitialize){
            enable_connections();
            mAdvertisingState=ADVERTISING_IS_ENABLING;
        }

        isFirstCallToInitialize=false;
        return true; //no error
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean enable_connections() {
        Log.d(TAG, "enable_connections()");
        log("Enable connections");
        return start_advertising();
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disable_connections() {
        Log.d(TAG, "disable_connections()");
        log("Disabling new connection");
        stop_advertising();
    }

    public void disconnect_all(){
        Collection<Anchor> cNetwork =  mNetwork.values();
        for (Anchor elem : cNetwork) {
            if(elem.mBluetoothGattClient != null){
                String device_label = elem.mBluetoothGattClient.getDevice().toString();
                mBluetoothGattServer.cancelConnection(elem.mBluetoothGattClient.getDevice());
                elem.mBluetoothGattClient.disconnect();
                elem.mBluetoothGattClient.close();
                elem.mBluetoothGattClient = null;

                //String intentAction = ACTION_GATT_DISCONNECTED;
                //broadcastUpdate(intentAction, device_label);
            }
        }

        mNetwork.clear();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        Log.d(TAG, "close()");
        stop_advertising();

        Collection<Anchor> cNetwork =  mNetwork.values();
        for (Anchor elem : cNetwork) {
            if(elem.mBluetoothGattClient != null){
                mBluetoothGattServer.cancelConnection(elem.mBluetoothGattClient.getDevice());
                elem.mBluetoothGattClient.disconnect();
                elem.mBluetoothGattClient.close();
                elem.mBluetoothGattClient = null;
            }
        }

        if(mBluetoothGattServer != null) {
            mBluetoothGattServer.close();
            mBluetoothGattServer = null;
        }

        //mNetwork.clear();
        //mNetwork = null;

        mBluetoothAdapter = null;
        mBluetoothManager = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
//    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
//        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
//            Log.w(TAG, "BluetoothAdapter not initialized");
//            return;
//        }
//        mBluetoothGatt.readCharacteristic(characteristic);
//    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
//    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
//                                              boolean enabled) {
//        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
//            Log.w(TAG, "BluetoothAdapter not initialized");
//            return;
//        }
//        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
//
//        // This is specific to Heart Rate Measurement.
//        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
//            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
//                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//            mBluetoothGatt.writeDescriptor(descriptor);
//        }
//    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
//    public List<BluetoothGattService> getSupportedGattServices() {
//        if (mBluetoothGatt == null) return null;
//
//        return mBluetoothGatt.getServices();
//    }

    public boolean checkBluetoothDistanceServiceSupported(String deviceLabel) {
        Anchor mAnchor = mNetwork.get(deviceLabel);
        if(mAnchor == null)
            return false;

        if(mAnchor.mRangeCharacteristic == null)
            return false;

        return true;
    }

    public HashMap<String, Anchor>  getNetork(){
        return mNetwork;
    }

    public boolean getAdvertisingState(){
        return mAdvertisingState==ADVERTISING_ENABLED || mAdvertisingState==ADVERTISING_IS_ENABLING;
    }


    private static float[] xs = {-10, -10, 10, 10};
    private static float[] ys = {-10, 10, -10, 10};


    private int dummy_idx = 0;
    public class Anchor{
        BluetoothGatt mBluetoothGattClient;
        BluetoothGattService mDistanceService;
        BluetoothGattCharacteristic mRangeCharacteristic;
        BluetoothDevice mBluetoothDevice;
        float last_distance;
        float x_pos;
        float y_pos;


        public Anchor(){
            this(null,null, null, null);
        }

        public Anchor(BluetoothDevice mBluetoothDevice){
            this(null,null, null, mBluetoothDevice);
        }

        public Anchor(BluetoothGatt mBluetoothGattClient,BluetoothGattService mDistanceService, BluetoothGattCharacteristic mRangeCharacteristic){
            this(mBluetoothGattClient,mDistanceService, mRangeCharacteristic, null);
        }

        public Anchor(BluetoothGatt mBluetoothGattClient,BluetoothGattService mDistanceService, BluetoothGattCharacteristic mRangeCharacteristic, BluetoothDevice mBluetoothDevice) {
            this.mBluetoothGattClient = mBluetoothGattClient;
            this.mDistanceService = mDistanceService;
            this.mRangeCharacteristic = mRangeCharacteristic;
            this.mBluetoothDevice = mBluetoothDevice;

            boolean pos_set = false;
            if (mBluetoothDevice != null) {
                float[] apos = AnchorPos.getPos(mBluetoothDevice.toString());
                if (apos != null) {
                    this.x_pos = apos[0];
                    this.y_pos = apos[1];
                    pos_set = true;
                }
            }

            if (!pos_set){
                this.x_pos = xs[dummy_idx % xs.length] * ((int) Math.floor(dummy_idx / xs.length) + 1);
                this.y_pos = ys[dummy_idx % xs.length] * ((int) Math.floor(dummy_idx / xs.length) + 1);
                dummy_idx++;
            }

            last_distance = 0f/0f; //this is NaN
        }

        public float getLastDistanceEstimation(){
            //if(mRangeCharacteristic == null)
            //    return NaN;
            //return ((double)(mRangeCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0)))/10;
            return last_distance;
        }
    }
}