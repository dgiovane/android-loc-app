package com.example.android.bluetoothlegatt;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class MovingCharacter extends GameObject {

    private static final int ROW_TOP_TO_BOTTOM = 0;
    private static final int ROW_RIGHT_TO_LEFT = 1;
    private static final int ROW_LEFT_TO_RIGHT = 2;
    private static final int ROW_BOTTOM_TO_TOP = 3;

    // Row index of Image are being used.
    private int rowUsing = ROW_LEFT_TO_RIGHT;

    private int colUsing;

    private Bitmap[] leftToRights;
    private Bitmap[] rightToLefts;
    private Bitmap[] topToBottoms;
    private Bitmap[] bottomToTops;

    // Velocity of game character (pixel/millisecond)
    private float VELOCITY = 0.2f;

    private int movingVectorX = 10;
    private int movingVectorY = 5;

    private long lastDrawNanoTime =-1;

    private GameSurface gameSurface;

    public MovingCharacter(GameSurface gameSurface, Bitmap image, int x, int y) {
        this(gameSurface, null,image,x,y);
    }

    public MovingCharacter(GameSurface gameSurface, String label, Bitmap image, int x, int y, float velocity) {
        this(gameSurface, label,image,x,y);

        this.VELOCITY = velocity;
    }

    public MovingCharacter(GameSurface gameSurface, String label, Bitmap image, int x, int y) {
        super(label, image, 4, 3, x, y);

        this.gameSurface= gameSurface;

        this.topToBottoms = new Bitmap[colCount]; // 3
        this.rightToLefts = new Bitmap[colCount]; // 3
        this.leftToRights = new Bitmap[colCount]; // 3
        this.bottomToTops = new Bitmap[colCount]; // 3

        for(int col = 0; col< this.colCount; col++ ) {
            this.topToBottoms[col] = this.createSubImageAt(ROW_TOP_TO_BOTTOM, col);
            this.rightToLefts[col]  = this.createSubImageAt(ROW_RIGHT_TO_LEFT, col);
            this.leftToRights[col] = this.createSubImageAt(ROW_LEFT_TO_RIGHT, col);
            this.bottomToTops[col]  = this.createSubImageAt(ROW_BOTTOM_TO_TOP, col);
        }
    }

    public Bitmap[] getMoveBitmaps()  {
        switch (rowUsing) {
            case ROW_BOTTOM_TO_TOP:
                return this.bottomToTops;
            case ROW_LEFT_TO_RIGHT:
                return this.leftToRights;
            case ROW_RIGHT_TO_LEFT:
                return this.rightToLefts;
            case ROW_TOP_TO_BOTTOM:
                return this.topToBottoms;
            default:
                return null;
        }
    }

    public Bitmap getCurrentMoveBitmap()  {
        Bitmap[] bitmaps = this.getMoveBitmaps();
        return bitmaps[this.colUsing];
    }


    public void update()  {
        this.colUsing++;
        if(colUsing >= this.colCount)  {
            this.colUsing =0;
        }
        // Current time in nanoseconds
        long now = System.nanoTime();

        // Never once did draw.
        if(lastDrawNanoTime==-1) {
            lastDrawNanoTime= now;
        }
        // Change nanoseconds to milliseconds (1 nanosecond = 1000000 milliseconds).
        int deltaTime = (int) ((now - lastDrawNanoTime)/ 1000000 );

        // Distance moves
        float distance = VELOCITY * deltaTime;

        double movingVectorLength = Math.sqrt(movingVectorX* movingVectorX + movingVectorY*movingVectorY);

        // Calculate the new position of the game character.
        this.x = x +  (int)(distance* movingVectorX / movingVectorLength);
        this.y = y +  (int)(distance* movingVectorY / movingVectorLength);

        // When the game's character touches the edge of the screen, then change direction

//        if(this.x < 0 )  {
//            this.x = 0;
//            this.movingVectorX = - this.movingVectorX;
//        } else if(this.x > this.gameSurface.getWidth() -width)  {
//            this.x= this.gameSurface.getWidth()-width;
//            this.movingVectorX = - this.movingVectorX;
//        }
//
//        if(this.y < 0 )  {
//            this.y = 0;
//            this.movingVectorY = - this.movingVectorY;
//        } else if(this.y > this.gameSurface.getHeight()- height)  {
//            this.y= this.gameSurface.getHeight()- height;
//            this.movingVectorY = - this.movingVectorY ;
//        }

        // rowUsing
        if( movingVectorX > 0 )  {
            if(movingVectorY > 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)) {
                this.rowUsing = ROW_TOP_TO_BOTTOM;
            }else if(movingVectorY < 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)) {
                this.rowUsing = ROW_BOTTOM_TO_TOP;
            }else  {
                this.rowUsing = ROW_LEFT_TO_RIGHT;
            }
        } else {
            if(movingVectorY > 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)) {
                this.rowUsing = ROW_TOP_TO_BOTTOM;
            }else if(movingVectorY < 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)) {
                this.rowUsing = ROW_BOTTOM_TO_TOP;
            }else  {
                this.rowUsing = ROW_RIGHT_TO_LEFT;
            }
        }
    }

    public void updateVector(int[] newPosition)  {

        int x = newPosition[0];
        int y = newPosition[1];

        int movingVectorX =x-  this.getX() ;
        int movingVectorY =y-  this.getY() ;

        this.setMovingVector(movingVectorX,movingVectorY);

    }

    public void draw(Canvas canvas)  {
//        Bitmap bitmap = this.getCurrentMoveBitmap();
////
////        Rect src = new Rect(0,0,bitmap.getWidth()-1, bitmap.getHeight()-1);
////        int left;
////        int top;
////        int right;
////        int bottom;
////
////        if(this.getObjectLabel().equals("MAP")){
////            left = (int)((x - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio());
////            top = (int)((y - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio());
////            right = (int)((x - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio())+(int)(this.width*this.gameSurface.getMapRatio())-1;
////            bottom = (int)((y - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio())+(int)(this.height*this.gameSurface.getMapRatio())-1;
////        }else{
////            int center_x = (int)((x - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio() + this.width/2);
////            int center_y = (int)((y - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio() + this.height/2);
////
////
////            left = center_x - this.width/2;
////            top = center_y - this.height/2;
////            right = center_x + this.width/2 - 1;
////            bottom = center_y + this.height/2 - 1;
////        }
////
////        Rect dest = new Rect(left, top,right, bottom);
////        canvas.drawBitmap(bitmap, src, dest, null);
////
////        this.lastDrawNanoTime= System.nanoTime();

        Bitmap bitmap = this.getCurrentMoveBitmap();

        Rect src = new Rect(0,0,bitmap.getWidth()-1, bitmap.getHeight()-1);
        int left;
        int top;
        int right;
        int bottom;

        int center_x = (int)((this.getX() - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio());
        int center_y = (int)((this.getY() - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio());

        if(this.getObjectLabel().equals("MAP")){ //map is scaled.
            left = center_x - (int)(this.getWidth()/(2*this.gameSurface.getMapRatio()));
            top = center_y - (int)(this.getHeight()/(2*this.gameSurface.getMapRatio()));
            right = center_x + (int)(this.getWidth()/(2*this.gameSurface.getMapRatio()) - 1);
            bottom = center_y + (int)(this.getHeight()/(2*this.gameSurface.getMapRatio()) - 1);
        }else{
            left = center_x - this.getWidth()/2;
            top = center_y - this.getHeight()/2;
            right = center_x + this.getWidth()/2 - 1;
            bottom = center_y + this.getHeight()/2 - 1;
        }

        Rect dest = new Rect(left, top, right, bottom);
        canvas.drawBitmap(bitmap, src, dest, null);

        this.lastDrawNanoTime= System.nanoTime();
    }

    public void setMovingVector(int movingVectorX, int movingVectorY)  {
        this.movingVectorX= movingVectorX;
        this.movingVectorY = movingVectorY;
    }
}