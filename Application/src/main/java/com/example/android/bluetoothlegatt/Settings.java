package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.util.Log;

import java.util.Locale;


public class Settings extends PreferenceActivity {

    private static final String TAG = "SettingsActivity";
    private static final int REQUEST_FILE = 1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference filePicker = (Preference) findPreference("filePicker");
        final EditTextPreference RealXSizePicker = (EditTextPreference) findPreference("width_m");
        final EditTextPreference RealYSizePicker = (EditTextPreference) findPreference("height_m");

        filePicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_FILE);
                return true;
            }
        });

        SharedPreferences preferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String mapPath = preferences.getString(getString(R.string.map_filename),getString(R.string.default_map_path));

        final String path_parts[] = mapPath.split("/");

        float width_stored = preferences.getFloat(getString(R.string.saved_map_width_pre_name)+path_parts[path_parts.length-1],52.4f);
        float height_stored = preferences.getFloat(getString(R.string.saved_map_height_pre_name)+path_parts[path_parts.length-1],82.79f);

        RealXSizePicker.setTitle(getString(R.string.alt_map_width_string_first_part)+ width_stored + " m");
        RealYSizePicker.setTitle(getString(R.string.alt_map_height_string_first_part)+ height_stored + " m");
        RealXSizePicker.setText(String.format(Locale.ENGLISH,"%.2f", width_stored));
        RealYSizePicker.setText(String.format(Locale.ENGLISH,"%.2f", height_stored));

        RealXSizePicker.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String width_string = (String)newValue;
                RealXSizePicker.setTitle(getString(R.string.alt_map_width_string_first_part)+ width_string + " m");
                RealXSizePicker.setText(width_string);

                try {
                    float width_new = Float.valueOf(width_string);
                    tryToUpdatePreferences(width_new,getString(R.string.saved_map_width_pre_name)+path_parts[path_parts.length-1]);

                    return true;
                }catch (Exception e){

                    return false;
                }
            }
        });

        RealYSizePicker.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String height_string = (String)newValue;
                RealYSizePicker.setTitle(getString(R.string.alt_map_height_string_first_part)+ height_string + " m");
                RealYSizePicker.setText(height_string);

                try {
                    float width_new = Float.valueOf(height_string);
                    tryToUpdatePreferences(width_new,getString(R.string.saved_map_height_pre_name)+path_parts[path_parts.length-1]);

                    return true;
                }catch (Exception e){

                    return false;
                }
            }
        });
    }

    private void tryToUpdatePreferences(float newValue, String pref_label){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putFloat(pref_label,newValue);
        editor.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //get the new value from Intent data
        if (requestCode == REQUEST_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    //imageview.setImageURI(selectedImage);
                    if(selectedImage == null){
                        Log.d(TAG,"selectedImage == null");
                    }
                    String newValue = getRealPath(selectedImage);

                    SharedPreferences preferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(getString(R.string.map_filename), newValue);
                    editor.commit();
                }
            }
        }
    }

    private String getRealPath(Uri contentURI) {
        try {//try this (works if image is selected from image selector)
            // Will return "image:x*"
            String wholeID = DocumentsContract.getDocumentId(contentURI);

// Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

// where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = getContentResolver().
                    query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);

            String filePath = "";

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            cursor.close();

            return filePath;
        }catch (Exception e){//otherwise try this (works if image is selected from file manager)
            try{
            String result;
            Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
            return result;
            }catch (Exception e2){
                return null;
            }
        }
    }
 /*
    private String getRealPath(Uri uri){
        try {
            // Will return "image:x*"
            String wholeID = DocumentsContract.getDocumentId(uri);

// Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

// where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = getContentResolver().
                    query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);

            String filePath = "";

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            cursor.close();

            return filePath;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }*/
}
