package com.example.android.bluetoothlegatt;

public interface AnchorCallbacks {
    public void onPositionChange (String label, float new_x, float new_y, float new_scale);
    //public void onSurfaceChange (String label, float new_x, float new_y, float new_scale);
}
