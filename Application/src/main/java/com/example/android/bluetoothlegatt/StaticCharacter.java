package com.example.android.bluetoothlegatt;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class StaticCharacter extends GameObject {
    private Bitmap charBitmap;

    private long lastDrawNanoTime =-1;

    private float realSizeX=-1; //size of the object in the real world given in [m]. Used mainly for the background. -1 if not provided
    private float realSizeY=-1;

    //private float ratio=1;

    private GameSurface gameSurface;

    public StaticCharacter(GameSurface gameSurface, String label, Bitmap image, int x, int y) {
        this(gameSurface, label,image,x,y, -1, -1);
    }

    public StaticCharacter(GameSurface gameSurface, String label, Bitmap image, int x, int y,  float realSizeX,  float realSizeY) {
        super(label, image, 1, 1, x, y);

        this.gameSurface= gameSurface;
        this.charBitmap = image;
        this.realSizeX = realSizeX;
        this.realSizeY = realSizeY;
    }

    public float[] getRealSize(){
        if(realSizeX == -1 || realSizeY == -1){
            return new float[]{(float) this.getWidth(), (float)this.getHeight()};
        }


        return new float[]{ this.realSizeX, this.realSizeY};
    }

    public boolean isStatic(){
        return true;
    }


    public Bitmap getBitmap()  {
        return this.charBitmap;
    }


    public void update()  {

    }

    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }

//    public void setRatio(float r){
//        this.ratio=r*this.ratio;
//    }

//    public float getRatio(){
//        return this.ratio;
//    }

    public void draw(Canvas canvas)  {
//        Bitmap bitmap = this.getBitmap();
//
//        Rect src = new Rect(0,0,bitmap.getWidth()-1, bitmap.getHeight()-1);
//        int left;
//        int top;
//        int right;
//        int bottom;
//
//        if(this.getObjectLabel().equals("MAP")){
//            left = (int)((this.getX() - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio());
//            top = (int)((this.getY() - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio());
//            right = (int)((this.getX() - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio())+(int)(this.getWidth()*this.gameSurface.getMapRatio())-1;
//            bottom = (int)((this.getY() - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio())+(int)(this.getHeight()*this.gameSurface.getMapRatio())-1;
//        }else{
//            int center_x = (int)((this.getX() - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio());
//            int center_y = (int)((this.getY() - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio());
//
//
//            left = center_x - this.getWidth()/2;
//            top = center_y - this.getHeight()/2;
//            right = center_x + this.getWidth()/2 - 1;
//            bottom = center_y + this.getHeight()/2 - 1;
//        }
//
//        Rect dest = new Rect(left, top, right, bottom);
//        canvas.drawBitmap(bitmap, src, dest, null);
//
//        this.lastDrawNanoTime= System.nanoTime();

        Bitmap bitmap = this.getBitmap();

        Rect src = new Rect(0,0,bitmap.getWidth()-1, bitmap.getHeight()-1);
        int left;
        int top;
        int right;
        int bottom;

        int center_x = (int)((this.getX() - this.gameSurface.getX_offset())*this.gameSurface.getMapRatio());
        int center_y = (int)((this.getY() - this.gameSurface.getY_offset())*this.gameSurface.getMapRatio());

        if(this.getObjectLabel().equals("MAP")){ //only map is scaled.
            left = center_x - (int)(this.getWidth()*this.gameSurface.getMapRatio()/2);
            top = center_y - (int)(this.getHeight()*this.gameSurface.getMapRatio()/2);
            right = center_x + (int)(this.getWidth()*this.gameSurface.getMapRatio()/2 - 1);
            bottom = center_y + (int)(this.getHeight()*this.gameSurface.getMapRatio()/2 - 1);
        }else{
            left = center_x - this.getWidth()/2;
            top = center_y - this.getHeight()/2;
            right = center_x + this.getWidth()/2 - 1;
            bottom = center_y + this.getHeight()/2 - 1;
        }

        Rect dest = new Rect(left, top, right, bottom);
        canvas.drawBitmap(bitmap, src, dest, null);

        this.lastDrawNanoTime= System.nanoTime();
    }

}