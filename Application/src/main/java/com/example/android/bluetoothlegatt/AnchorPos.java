package com.example.android.bluetoothlegatt;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class AnchorPos {
    private static final Map<String, float[]> anchorsMap;

    static {
        HashMap<String, float[]> aMap = new HashMap<String, float[]>();
        aMap.put("E1:9B:07:10:AC:14", new float[]{10, 10});
        aMap.put("CF:4F:7E:F0:23:17", new float[]{10, 20});
        aMap.put("CA:30:EF:AF:A7:1D", new float[]{20, 10});
        aMap.put("D4:EF:22:19:4B:A8", new float[]{20, 20});
        anchorsMap = Collections.unmodifiableMap(aMap);
    }

    static float[] getPos(String label){
        return anchorsMap.get(label);
    }
}
