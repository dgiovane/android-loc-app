
Android Sample
===================================

This sample demonstrates how to use time of flight over ble on Android devices.
To make this sample work you need 3-4 PCA10040 (or PCA10056 or Thingy:52 by Nordic Semiconductor) flashed with the firmware found in https://bitbucket.org/dgiovane/time_of_flight_ble/ .
These boards act as reference anchors and measure their own distance to the Android device, then this distance is sent to the Android device through the BLE link that is used also to perform the distance measurement.

Not all the Android devices are adapt for this use case since, for the purpose of the time of flight measurement the smartphone/tablet have to assume the BLE PERIPHERAL role in the connection, and this kind of connection has to be maintained with all the reference anchors simultaneously (concurrently).
Hardware and software constraints limits the number of concurrent connection that the device can handle, I obtained good results with an HTC Nexus9 tablet, while on Nexus5x only 3 anchors can be connected, other devices are NOT TESTED!